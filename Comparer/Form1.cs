﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comparer
{
    public partial class Form1 : Form
    {
        List<FileChange> changes;
        List<ListViewItem> lvis;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonStart_Click( object sender, EventArgs e )
        {
            string path1 = textLeft.Text;
            string path2 = textRight.Text;
            if ( !path1.EndsWith( "\\" ) ) path1 = path1 + "\\";
            if ( !path2.EndsWith( "\\" ) ) path2 = path2 + "\\";
            if ( !Directory.Exists( path1 ) || !Directory.Exists( path2 ) )
            {
                return;
            }

            new Thread( DoDiff ).Start( new[] { path1, path2 } );

        }

        void DoDiff( object obj )
        {
            string[] os = (string[])obj;
            string path1 = os[0];
            string path2 = os[1];

            string[] files2 = Directory.GetFiles( path2, "*.*", SearchOption.AllDirectories );

            changes = new List<FileChange>();
            foreach ( var file in files2 )
            {
                var name = file.Substring( path2.Length );
                var original = path1 + name;

                var ch = new FileChange { File = name };
                if ( !File.Exists( original ) )
                {
                    ch.Mode = ChangeMode.Add;
                    changes.Add( ch );
                }
                else
                {
                    ch.Mode = ChangeMode.Modify;
                    if ( IsDiff( file, original ) )
                        changes.Add( ch );
                }
            }

            string[] files1 = Directory.GetFiles( path1, "*.*", SearchOption.AllDirectories );
            foreach ( var file in files1 )
            {
                var name = file.Substring( path1.Length );
                var @new = path2 + name;

                if ( !File.Exists( @new ) )
                {
                    changes.Add( new FileChange
                    {
                        File = name,
                        Mode = ChangeMode.Remove
                    } );
                }
            }

            changes.Sort( ( t1, t2 ) => t1.File.CompareTo( t2.File ) );

            var list = new List<ListViewItem>();

            foreach ( var change in changes )
            {
                var file1 = new FileInfo( path1 + change.File );
                var file2 = new FileInfo( path2 + change.File );
                string[] ss;
                int key;
                if ( change.Mode == ChangeMode.Add )
                {
                    ss = new[] { change.File, file2.Length.ToString(), file2.LastWriteTime.ToString( "MM/dd/yyyy HH:mm:ss" ) };
                    key = 0;
                }
                else if ( change.Mode == ChangeMode.Remove )
                {
                    ss = new[] { change.File, file1.Length.ToString(), file1.LastWriteTime.ToString( "MM/dd/yyyy HH:mm:ss" ) };
                    key = 1;
                }
                else
                {
                    ss = new[] { change.File, file2.Length.ToString(), file2.LastWriteTime.ToString( "MM/dd/yyyy HH:mm:ss" ) };
                    key = -1;
                }

                list.Add( new ListViewItem( ss, key ) );
            }

            lvis = list;
            this.BeginInvoke( new Action( delegate
            {
                listRight.VirtualListSize = lvis.Count;
            } ) );
        }

        private void listRight_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( listRight.SelectedIndices.Count < 1 )
                return;
            int selected = listRight.SelectedIndices[0];
            if ( selected >= changes.Count )
                return;

            string path1 = textLeft.Text;
            string path2 = textRight.Text;
            if ( !path1.EndsWith( "\\" ) ) path1 = path1 + "\\";
            if ( !path2.EndsWith( "\\" ) ) path2 = path2 + "\\";
            if ( !Directory.Exists( path1 ) || !Directory.Exists( path2 ) )
            {
                return;
            }
            if ( !File.Exists( textDiffTool.Text ) )
                return;

            var change = changes[selected];
            if ( change.Mode == ChangeMode.Modify )
            {
                Process.Start( textDiffTool.Text, string.Format( "\"{0}\" \"{1}\"", path1 + change.File, path2 + change.File ) );
            }
            else
            {
                Process.Start( "notepad.exe", ( change.Mode == ChangeMode.Add ? path2 : path1 ) + change.File );
            }
        }

        bool IsDiff( string file1, string file2 )
        {
            using ( var reader1 = File.OpenText( file1 ) )
            using ( var reader2 = File.OpenText( file2 ) )
            {
                string s1, s2;
                do
                {
                    s1 = reader1.ReadLine();
                    s2 = reader2.ReadLine();
                    if ( s1 != s2 )
                        return true;
                } while ( s1 != null || s2 != null );
            }
            return false;
        }

        private void listRight_RetrieveVirtualItem( object sender, RetrieveVirtualItemEventArgs e )
        {
            if ( lvis == null || e.ItemIndex >= lvis.Count )
                return;

            e.Item = lvis[e.ItemIndex];
        }

        private void Form1_Load( object sender, EventArgs e )
        {
        }
    }

    class FileChange
    {
        public string File { get; set; }
        public ChangeMode Mode { get; set; }
    }

    enum ChangeMode
    {
        Add,
        Remove,
        Modify
    }

    class ListViewEx : ListView
    {
        public ListViewEx() : base()
        {
            SetStyle( ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true );
        }
    }
}
